
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author commis
 */
public class GameOX {

    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean isFinish = false;
    static char winner = '-';
    static int count = 0;

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                count++;
                break;
            }
            System.out.println("Table at row and col is not empty !!");
        }

    }

    static void checkcol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkCrossX1() {
        if (table[0][0] == 'X') {
            if (table[1][1] == 'X') {
                if (table[2][2] == 'X') {
                    isFinish = true;
                    winner = player;
                }
            }
        }
    }

    static void checkCrossX2() {
        if (table[0][2] == 'X') {
            if (table[1][1] == 'X') {
                if (table[2][0] == 'X') {
                    isFinish = true;
                    winner = player;
                }
            }
        }
    }

    static void checkCrossO1() {
        if (table[0][0] == 'O') {
            if (table[1][1] == 'O') {
                if (table[2][2] == 'O') {
                    isFinish = true;
                    winner = player;
                }
            }
        }
    }

    static void checkCrossO2() {
        if (table[0][2] == 'O') {
            if (table[1][1] == 'O') {
                if (table[2][0] == 'O') {
                    isFinish = true;
                    winner = player;
                }
            }
        }
    }

    static void checkDraw() {
        if (winner == '-' && count == 9) {
            isFinish = true;
        }
    }

    static void CheckWin() {
        checkRow();
        checkcol();
        checkCrossX1();
        checkCrossX2();
        checkCrossO1();
        checkCrossO2();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else if (player == 'O') {
            player = 'X';
        }
    }

    static void showResult() {
        showTable();
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ....");
    }

    public static void main(String[] args) {
        do {
            showWelcome();
            showTable();
            showTurn();
            input();
            CheckWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
